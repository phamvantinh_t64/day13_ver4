<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Student</title>
</head>
<body>
    <?php
        $data = $_SESSION["data"];
        include ("../variable.php");
    ?>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="center">
        <div class="container">            
            <div class="wrapper">
                <div class="label-row label-row-submit name">
                    <button>Họ và tên</button>
                    <h4><?php echo $data["name"] ?></h4>
                </div>
                <div class="label-row label-row-submit gender">
                    <button>Giới tính</button>
                    <h4><?php echo $genderArray[$data["gender"]] ?></h4>
                </div>
                <div class="label-row label-row-submit department">
                    <button>Phân khoa</button>
                    <h4><?php echo $departments[$data["department"]] ?></h4>
                </div>
                <div class="label-row label-row-submit date">
                    <button>Ngày sinh</button>
                    <h4><?php echo $data["date"] ?></h4>
                </div>
                <div class="label-row label-row-submit address">
                    <button>Địa chỉ</button>
                    <h4><?php echo $data["address"] ?></h4>
                </div>
                <div class="label-row label-row-submit image-submit">
                    <button>Hình ảnh</button>
                    <?php 
                    if (!empty($data["imageFile"])){
                    ?>
                        <img src="<?php echo $data["imageFile"] ?>" alt="">
                    <?php
                    }
                    ?>
                </div>
                <form method="post" action="complete_regist.php">
                    <div class="submit">
                        <input type="submit" value="Xác nhận">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<style>
<?php 
    include '../styles/global.css'; 
    include '../styles/register.css'; 
    ?>
</style>
</html>